This git repo represent the project lead by Vidal Attias and Anas El Moussaoui for the course 2.26.2 Web Data Management of the MPRI master's.

It consists in a website displaying a 2D map of the contiguous United States with a temperature overlay indicating average temperature in each parts of the country.
It provides several features such as
- Chosing a different period of the year for the weather data : the average temperature for each month of the year or an annual average temperature.
- Chosing three temperature thresholds corresponding to the user's whises:
	- Tmin : it represents the lowest acceptable temperature, i.e. all places with temperatures below Tmin will be in plain blue indicating the user should not go there.
	- Tmax : same as Tmin but for the maximum acceptable temperature. All places with temparatures above will be markes with a plain red color.
	- Tmid : the temperature the user would actually like to experience. Places with this exact temperature will be colored in green and all other temperatures will have a linear gradient from blue to green or green to red depending on whether they have temperatures below or above Tmid.
- Indications of isotherms in the country, i.e. the borders between acceptable and non-acceptable areas.

This web application has been developped using the Django back end technology and the Leaflet Javascript framework for displaying maps. The weather data have been extracted from weatherbase.com website.

In order to run the website, go to the root of the git repo and run the command
```
python3 manage.py runserver
```
which will run a local server. It should display

```
Starting development server at http://127.0.0.1:8000/
```

Important : in order to work, some Python packages must be installed : `numpy`, `matplotib`, `scipy`and `dms2dec`.

The important code is located in the following files :
`/static/map.js` is the code dedicated to displaying the map using the Leaflet framework.
`/temperature/templates/temperature/index.html` is the HTML code of the website
`/temperature/views.py` is the code dedicated to extracting data from `/weatherbase.csv` and running Delaunay triangulation and colors computation.
