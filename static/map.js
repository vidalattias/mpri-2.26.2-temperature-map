var mymap = L.map('mapid').setView([38.063797, -96.369276], 5);




L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
  maxZoom: 18,
  attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, ' +
    'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
  id: 'mapbox/streets-v9',
  tileSize: 512,
  zoomOffset: -1
}).addTo(mymap);

n = 100

hashCode = function(s) {
  return s.split("").reduce(function(a, b) {
    a = ((a << 5) - a) + b.charCodeAt(0);
    return a & a
  }, 0);
}

var colors = ["red", "blue", "black", "yellow", "green", "orange", "pink", "purple", "gold", "white"]


/*
$.getJSON('http://vidal-attias.io/cities.json', function(data) {
  console.log(data.length)
  for (var i = 0; i < data.length; i++) {
    L.circle([data[i]["Latitude"], data[i]["Longitude"]], {
      radius: 15000,
      color: colors[hashCode(data[i]["State"]) % colors.length],
    }).addTo(mymap);
  }
});
*/

mymap.createPane('temp');
mymap.getPane('temp').style.zIndex = 650;

mymap.createPane('states');
mymap.getPane('states').style.zIndex = 950;

mymap.createPane('bounds');
mymap.getPane('bounds').style.zIndex = 750;

var month = document.getElementById("month").value
var tmin = document.getElementById("tmin").value
var tmid = document.getElementById("tmid").value
var tmax = document.getElementById("tmax").value

$.getJSON("http://127.0.0.1:8000/json_temp/"+month+"/"+tmin+"/"+tmid+"/"+tmax, function(data) {
  var points = data["points"]
  for (var i = 0; i < data["simplices"].length; i++) {
    var coordinates = data["simplices"][i]["c"]
    var temperature = data["simplices"][i]["t"]
    var latlngs = [points[coordinates[0]], points[coordinates[1]], points[coordinates[2]]];

    var polygon = L.polygon(latlngs, {fillColor: temperature, opacity: 0, fillOpacity : 1, zIndexOffset : 100, pane:"temp"}).addTo(mymap);
  }
});



$.getJSON("http://127.0.0.1:8000/json_upper/"+month+"/"+tmin+"/"+tmid+"/"+tmax, function(data) {
  for( var i = 0; i < data.length; i++)
  {
    var latlngs = [data[i][0], data[i][1]]
    console.log(latlngs)
    var polyline = L.polyline(latlngs, {color: 'yellow', weight : 5, pane:'bounds'}).addTo(mymap);
  }
});

$.getJSON("http://127.0.0.1:8000/json_lower/"+month+"/"+tmin+"/"+tmid+"/"+tmax, function(data) {
  for( var i = 0; i < data.length; i++)
  {
    var latlngs = [data[i][0], data[i][1]]
    console.log(latlngs)
    var polyline = L.polyline(latlngs, {color: 'turquoise', weight : 5, pane:'bounds'}).addTo(mymap);
  }
});


/*
$.getJSON('http://vidal-attias.io/low_bounds.json', function(data) {
  for( var i = 0; i < data.length; i++)
  {
    //console.log(data[i])
    var latlngs = [data[i][0], data[i][1]]
    var polyline = L.polyline(latlngs, {color: 'black', weight : 5, pane:'bounds'}).addTo(mymap);

  }
});


$.getJSON('http://vidal-attias.io/mid_bounds.json', function(data) {
  for( var i = 0; i < data.length; i++)
  {
    //console.log(data[i])
    var latlngs = [data[i][0], data[i][1]]
    //var polyline = L.polyline(latlngs, {color: 'green', weight : 5, pane:'bounds'}).addTo(mymap);
  }
});

*/

L.geoJson(statesData, {color:"black", fillOpacity:0, opacity: 1, pane:"states"}).addTo(mymap);
