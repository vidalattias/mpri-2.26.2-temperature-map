from django.http import HttpResponse
from django.shortcuts import render

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from scipy.spatial import Delaunay
import random
import numpy as np
import math
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from dms2dec.dms_convert import dms2dec

def index(request, month, tmin, tmid, tmax):
    tmin = int(tmin)
    tmid = int(tmid)
    tmax = int(tmax)

    str_ret = ""
    str_ret += "Month : " + str(month) + "<br />"
    str_ret += "tmin : " + str(tmin) + "<br />"
    str_ret += "tmid : " + str(tmid) + "<br />"
    str_ret += "tmax : " + str(tmax) + "<br />"

    context = {
                "month" : month,
                "tmin" : tmin,
                "tmid" : tmid,
                "tmax" : tmax
                }

    return render(request, 'temperature/index.html', context)

def default(request):
    return index(request, "september", 10, 20, 25)


def json_temp(request, month, tmin, tmid, tmax):
    tmin = int(tmin)
    tmid = int(tmid)
    tmax = int(tmax)
    month_dic = {
        "avergage" : 3,
        "january" : 4,
        "ferbruary" : 5,
        "march" : 6,
        "april" : 7,
        "may" : 8,
        "june" : 9,
        "july" : 10,
        "august" : 11,
        "september" : 12,
        "october" : 13,
        "november" : 14,
        "december" : 15,
    }

    xx = []
    yy = []
    t = []
    points = []

    file = open('weatherbase.csv')
    file.readline()

    for line in file:
        data = line.replace('\n','').split(';')

        if data[3] == '---':
            continue

        x = float(data[1])
        y = float(data[2])

        if len(data) < 16:
            continue


        xx.append(x)
        yy.append(y)
        #print(len(data))
        #print(data[0])
        t.append(float(data[month_dic[month]]))
        points.append([x,y])


    points = np.array(points)
    tri = Delaunay(points)




    def colorFader(c1,c2, mix, tmin, tmid, tmax): #fade (linear interpolate) from color c1 (at mix=0) to c2 (mix=1)
        '''
        mix = (mix-min(t))/(max(t)-min(t))
        c1=np.array(mpl.colors.to_rgb(c1))
        c2=np.array(mpl.colors.to_rgb(c2))
        return mpl.colors.to_hex((1-mix)*c1 + mix*c2)
        '''

        if mix < tmin:
            return "blue"
        if mix >= tmax:
            return 'red'
        if mix < tmid:

            mix = (mix-tmin)/(tmid-tmin)
            cp1=np.array(mpl.colors.to_rgb(c1))
            cp2=np.array(mpl.colors.to_rgb(c2))

            return mpl.colors.to_hex((1-mix)*cp1 + mix*cp2)
        else:
            mix = (mix-tmid)/(tmax-tmid)
            cp1=np.array(mpl.colors.to_rgb(c2))
            cp2=np.array(mpl.colors.to_rgb(c3))

            return mpl.colors.to_hex((1-mix)*cp1 + mix*cp2)

    c1='blue' #blue
    c2 = 'green'
    c3='red' #green





    json_points = '{"points" : ['

    for point in points:
        json_points += "["+str(point[0]) + "," + str(point[1])+"],"

    json_points = json_points[:-1] + '],"simplices":['

    json_triangle = ""

    for x in tri.simplices:
        #{"coordinates" : [], "temperateure : "}

        #json_triangle += '{"t":' + str(int(np.mean([t[x[0]], t[x[1]], t[x[2]]]))) + ', "c" :'
        temperature_mean = int(np.mean([t[x[0]], t[x[1]], t[x[2]]]))
        json_triangle += '{"t":"' + colorFader(c1, c2, temperature_mean, tmin, tmid, tmax) + '", "c" :'
        json_triangle += "["+str(x[0]) + "," + str(x[1]) + "," + str(x[2])+"]},"

    json_points += json_triangle[:-1] + ']}'
    return HttpResponse(json_points)


def json_upper(request, month, tmin, tmid, tmax):
    tmin = int(tmin)
    tmid = int(tmid)
    tmax = int(tmax)

    month_dic = {
        "avergage" : 3,
        "january" : 4,
        "ferbruary" : 5,
        "march" : 6,
        "april" : 7,
        "may" : 8,
        "june" : 9,
        "july" : 10,
        "august" : 11,
        "september" : 12,
        "october" : 13,
        "november" : 14,
        "december" : 15,
    }

    xx = []
    yy = []
    t = []
    points = []

    file = open('weatherbase.csv')
    file.readline()

    for line in file:
        data = line.replace('\n','').split(';')

        if data[3] == '---':
            continue

        x = float(data[1])
        y = float(data[2])

        if len(data) < 16:
            continue


        xx.append(x)
        yy.append(y)
        #print(len(data))
        #print(data[0])
        t.append(float(data[month_dic[month]]))
        points.append([x,y])


    points = np.array(points)
    tri = Delaunay(points)


    ts = []
    for x in tri.simplices:
        ts.append(np.mean([t[x[0]], t[x[1]], t[x[2]]]))


    upper_bound = []
    for i,x in enumerate(tri.simplices):
        tx = ts[i]
        neighbors = tri.neighbors[i]
        if neighbors[0] != -1:
            t0 = ts[neighbors[0]]
            if t0 < tmax and tx > tmax or t0 > tmax and tx < tmax:
                upper_bound.append([points[x[1]], points[x[2]]])

        if neighbors[1] != -1:
            t0 = ts[neighbors[1]]
            if t0 < tmax and tx > tmax or t0 > tmax and tx < tmax:
                upper_bound.append([points[x[2]], points[x[0]]])

        if neighbors[2] != -1:
            t0 = ts[neighbors[2]]
            if t0 < tmax and tx > tmax or t0 > tmax and tx < tmax:
                upper_bound.append([points[x[1]], points[x[0]]])


    upper_boud_json = "["

    for x in upper_bound:
        upper_boud_json += "[[" + str(x[0][0]) + "," + str(x[0][1]) + "],[" + str(x[1][0]) + "," + str(x[1][1]) + "]],"

    upper_boud_json = upper_boud_json[:-1] + "]"

    return HttpResponse(upper_boud_json)


def json_lower(request, month, tmin, tmid, tmax):
    tmin = int(tmin)
    tmid = int(tmid)
    tmax = int(tmax)

    month_dic = {
        "avergage" : 3,
        "january" : 4,
        "ferbruary" : 5,
        "march" : 6,
        "april" : 7,
        "may" : 8,
        "june" : 9,
        "july" : 10,
        "august" : 11,
        "september" : 12,
        "october" : 13,
        "november" : 14,
        "december" : 15,
    }

    xx = []
    yy = []
    t = []
    points = []

    file = open('weatherbase.csv')
    file.readline()

    for line in file:
        data = line.replace('\n','').split(';')

        if data[3] == '---':
            continue

        x = float(data[1])
        y = float(data[2])

        if len(data) < 16:
            continue


        xx.append(x)
        yy.append(y)
        #print(len(data))
        #print(data[0])
        t.append(float(data[month_dic[month]]))
        points.append([x,y])


    points = np.array(points)
    tri = Delaunay(points)


    ts = []
    for x in tri.simplices:
        ts.append(np.mean([t[x[0]], t[x[1]], t[x[2]]]))


    lower_boud = []
    for i,x in enumerate(tri.simplices):
        tx = ts[i]
        neighbors = tri.neighbors[i]
        if neighbors[0] != -1:
            t0 = ts[neighbors[0]]
            if t0 < tmin and tx > tmin or t0 > tmin and tx < tmin:
                lower_boud.append([points[x[1]], points[x[2]]])

        if neighbors[1] != -1:
            t0 = ts[neighbors[1]]
            if t0 < tmin and tx > tmin or t0 > tmin and tx < tmin:
                lower_boud.append([points[x[2]], points[x[0]]])

        if neighbors[2] != -1:
            t0 = ts[neighbors[2]]
            if t0 < tmin and tx > tmin or t0 > tmin and tx < tmin:
                lower_boud.append([points[x[1]], points[x[0]]])


    lower_boud_json = "["

    for x in lower_boud:
        lower_boud_json += "[[" + str(x[0][0]) + "," + str(x[0][1]) + "],[" + str(x[1][0]) + "," + str(x[1][1]) + "]],"

    lower_boud_json = lower_boud_json[:-1] + "]"

    return HttpResponse(lower_boud_json)
