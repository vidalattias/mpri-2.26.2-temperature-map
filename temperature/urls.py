from django.urls import path

from . import views

urlpatterns = [
    path('<str:month>/<str:tmin>/<str:tmid>/<str:tmax>/', views.index, name='index'),
    path('json_temp/<str:month>/<str:tmin>/<str:tmid>/<str:tmax>/', views.json_temp),
    path('json_upper/<str:month>/<str:tmin>/<str:tmid>/<str:tmax>/', views.json_upper),
    path('json_lower/<str:month>/<str:tmin>/<str:tmid>/<str:tmax>/', views.json_lower),
    path('', views.default)
]
